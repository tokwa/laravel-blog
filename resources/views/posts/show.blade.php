@extends('layouts.app')

@section('content')
<header id="section-header-update">

</header>
    
<section id="section-update-1">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="card" style="width:30rem;">
                    <img class="card-img-top" src="{{asset('images/saitama.jpg')}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <p class="card-text">{{$post->body}}</p>
                    <a href="{{route('posts.index')}}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
