@extends('layouts.app')

@section('content')

<head id="section-header-create">

</head>

<section id="section-create-1">

<div class="container">
    <form method="POST" action="{{route('posts.store')}}">
        {{ csrf_field() }}
            <div class="form-group">
            <label for="title">Post Title</label>
            <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" id="title" name="title" aria-describedby="title-Help" placeholder="Post title..." value="{{old('title')}}">
            <small id="titleHelp" class="form-text text-muted">Title of the post.</small>
                @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="body">Post Body</label>
            <textarea class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}" id="body" name="body" rows="3">{{old('body')}}</textarea>
                <small id="titleHelp" class="form-text text-muted">Post content.</small>
                @if ($errors->has('body'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</section>
@endsection