@extends('layouts.app')

@section('content')

<header id="section-header-edit">

</header>

<section id="section-edit-1">
<div class="container">
    <div class="container">
        <form method="POST" action="/posts/{{$post->id}}">
            {{method_field('PATCH')}}
            {{ csrf_field() }}
                <div class="form-group">
                <label for="title">Post Title</label>
                <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" id="title" name="title" aria-describedby="title-Help" placeholder="Post title..." value="{{$post->title}}">
                <small id="titleHelp" class="form-text text-muted">Title of the post.</small>
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="body">Post Body</label>
                <textarea class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}" id="body" name="body" rows="3">{{$post->body}}</textarea>
                    <small id="titleHelp" class="form-text text-muted">Post content.</small>
                    @if ($errors->has('body'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                    @endif
                </div>
                <a class="btn btn-danger" href="{{route('posts.index')}}">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
</section>
@endsection
