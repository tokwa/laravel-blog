<?php

namespace App\Http\Controllers;
use App\Post;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {

           $posts = Post::orderBy('created_at', 'desc')->paginate(5);
           
           return view('index', compact('posts'));
    }
}
